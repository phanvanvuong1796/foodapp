package com.phanvuong.food.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.phanvuong.food.DetailsRecipesActivity;
import com.phanvuong.food.MainActivity;
import com.phanvuong.food.R;
import com.phanvuong.food.adapter.ListRecipesAdapter;

public class RecipesFragment extends Fragment implements ListRecipesAdapter.ICallBack{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private MainActivity mainActivity;

    public RecipesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipes, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.list_item_recipes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ListRecipesAdapter adapter = new ListRecipesAdapter();
        adapter.setmICallBack(this);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity){
            mainActivity = (MainActivity) context;
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(mainActivity, DetailsRecipesActivity.class);
        startActivity(intent);
    }
}
