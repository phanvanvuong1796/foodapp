package com.phanvuong.food.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phanvuong.food.R;

import java.util.Random;

/**
 * Created by bongm on 2017-07-11.
 */

public class ListRecipesAdapter extends RecyclerView.Adapter<ListRecipesAdapter.CustomViewHolder>{

    private ICallBack mICallBack;

    public void setmICallBack(ICallBack mICallBack) {
        this.mICallBack = mICallBack;
    }

    String[] foodName = new String[]{"Mandarian Sorbet", "Blackberry Sorbet", "Mango Sorbet", "Strawberry Sorbet", "Apple Sorbet", "Sweet Sorbet"};
    String[] userName = new String[]{"John Doe", "Stansil Kirilov", "Steve Thomas", "Mark Kratzman"};
    int[] idImg = new int[]{R.mipmap.img1, R.mipmap.img2, R.mipmap.img3, R.mipmap.img4, R.mipmap.img5, R.mipmap.img6};

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_recipes, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        final int pos = position;
        Random random = new Random();
        holder.txtUserName.setText(userName[random.nextInt(userName.length-1)]);
        holder.txtFoodName.setText(foodName[random.nextInt(foodName.length-1)]);
        holder.imgFoodRecipes.setBackgroundResource(idImg[random.nextInt(idImg.length-1)]);
        holder.imgAvatar.setBackgroundResource(R.mipmap.user1);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mICallBack.onItemClick(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{

        TextView txtFoodName;
        TextView txtUserName;
        ImageView imgFoodRecipes;
        ImageView imgAvatar;
        View itemView;

        public CustomViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            txtFoodName = itemView.findViewById(R.id.txt_food_name);
            txtUserName = itemView.findViewById(R.id.txt_user_name);
            imgFoodRecipes = itemView.findViewById(R.id.img_food_recipes);
            imgAvatar = itemView.findViewById(R.id.img_avatar);
        }

    }

    public interface ICallBack{
        public void onItemClick(int position);
    }
}
